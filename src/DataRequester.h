// -*- c++ -*-
// $Id:$ 
//
// $Log:$
//
#ifndef DATAREQUESTER_H
#define DATAREQUESTER_H

#include <chrono>
#include <mutex>
#include <map>
#include <set>
#include <queue>
#include <condition_variable>

#include "tbb/concurrent_queue.h"

#include <boost/thread/thread.hpp>

#include "ROSasyncmsg/DataRequestMsg.h"

#include "ROSClear.h"


#include "asyncmsg/Session.h"
#include "ROSIO/TriggerGenerator.h"
#include "is/infodictionary.h"

class ISCallbackEvent;
class ISInfoReceiver;
namespace ROS {
   class DataRequester;

   class RequestGenerator:  public TriggerGenerator {
   public:
      RequestGenerator(std::vector<unsigned int>& channelList,
                       std::vector<uint32_t>& roiDistribution,
                       float l2Percent, float ebPercent, float metPercent,
                       int deleteGrouping,
                       unsigned int l1Rate, unsigned int maxL1,
                       unsigned int eventsPerEcr,
                       unsigned int maxOutstanding,
                       bool syncWithIS,
                       bool preloaded,
                       std::string partitionName,
                       std::vector<std::string>& otherTesters,
                       std::string myName,                       
                       std::shared_ptr<ROSClear> clearSession
         );

      virtual ~RequestGenerator();

      static void callback(ISCallbackEvent*);
      void registerRequester(DataRequester* requester);
      void setInstance(int instance,int totalInstances);

      void receivedL2Fragment(unsigned int level1Id);
      void receivedEBFragment(unsigned int level1Id);

      uint64_t l2RequestsGenerated();
      uint64_t ebRequestsGenerated();
      uint32_t wrapCount();
      void syncL1(uint32_t level1Id);
      void start();
      void stop();
      std::vector<unsigned int>* channels();
      virtual bool condition() override;
      bool inCurrentRange(unsigned int);
      void dropEBRequest(unsigned int level1Id);
      void dump();
   private:
      void printSet(std::set<unsigned int>& data);

      virtual void l2Request(const unsigned int level1Id,
                             const std::vector<unsigned int> * rols,
                             const unsigned int destination,
                             const unsigned int transactionId=1) override;
      virtual void ebRequest(const unsigned int l1Id,
                             const unsigned int destination) override;

      void processPendingRelease(unsigned int);

      virtual void releaseQueuePush(unsigned int level1Id) override;

      virtual void releaseRequest(const std::vector<unsigned int>* level1Ids,
                                  const unsigned int oldestL1id) override;

      virtual void ebQueuePush(unsigned int lvl1id) override;
      virtual unsigned int ebQueuePop(void) override;
      virtual int  ebQueueSize(void) override;
      virtual bool ebQueueEmpty(void) override;

      virtual unsigned int  releaseQueuePop(void) override;
      virtual int  releaseQueueSize(void) override;
      virtual bool releaseQueueEmpty(void) override;

      bool m_termFlag;
      int m_numberOfOutstanding;
      bool m_printData;
#ifdef TBB_QUEUE
      tbb::concurrent_bounded_queue<int> m_ebQueue;
#else
      std::queue<unsigned int>  m_ebQueue;
#endif
//      std::queue<unsigned int>  m_releaseQueue;
      unsigned int m_clearSequence;
      int m_metInterval;
      unsigned int m_l2RequestsGenerated;
      unsigned int m_metID;
      unsigned int m_metRequestsGenerated;

      std::mutex m_ebqMutex;
      std::mutex m_relqMutex;

      std::shared_ptr<ROSClear> m_clearSession;
      std::vector<DataRequester*> m_requester;
      unsigned int m_nextRequester;
      int m_metThreshold;
      uint64_t m_l2Generated;
      uint64_t m_ebGenerated;
      uint64_t m_clearGenerated;
      unsigned int m_ebTransactionId;

      std::chrono::time_point<std::chrono::system_clock> m_intervalStart;
      unsigned int m_l1Interval;
      uint32_t m_l1Rate; // target L1 rate in kHz

      uint32_t m_blockMargin;
      uint32_t m_highestReceived;

      std::set<uint32_t> m_pendingRelease;
      std::set<uint32_t> m_wrappedPending;
      std::set<uint32_t> m_outstandingRequest;

      bool m_wrapping;
      uint32_t m_wrapCount;
      bool m_wrapSynced;
      unsigned int m_maxL1;
      unsigned int m_initialL1;
      bool m_syncWithIS;
      bool m_preloaded;
      std::mutex m_mutex;
      std::condition_variable m_condition;
      std::vector<unsigned int> m_channelList;

      std::vector<uint64_t>m_otherSync;
      uint64_t m_syncPoint;
      uint64_t m_lastSyncPoint;
      ISInfoDictionary m_dictionary;
      std::vector<std::string> m_otherTesters;
      std::string m_isInfoName;
      ISInfoReceiver* m_infoReceiver;
   };

   class DataRequester: public daq::asyncmsg::Session {

   public:

      DataRequester(boost::asio::io_service& io_service,
                    bool printReq, bool printData,
                    int nThreads,
                    int maxOutstanding, int outsandingUnblock,
                    RequestGenerator* generator);
      ~DataRequester();

      void connect(boost::asio::ip::tcp::endpoint endpoint,
                   const std::string& myname="DataRequester");
      void disconnect();

      uint32_t outstanding();
      double megabytesReceived();
      uint64_t fragmentsWithError();

      void sendRequest(std::unique_ptr<DataRequestMsg> requestMsg);

      void debug();


      uint64_t reRequests();

//------------------
//    void generate(int nRequests=1,std::uint32_t l1Id=0);

//       float m_ebPercent;
//       float m_metPercent;
//       std::vector<unsigned int> m_channelList;
//       unsigned int m_clearSequence;
//       std::vector<unsigned int>* m_deletes;
//----------

   private:
      virtual std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(
         std::uint32_t typeId, std::uint32_t transactionId,
         std::uint32_t size) noexcept override;
      virtual void onOpen() noexcept override;
      virtual void onOpenError(const boost::system::error_code& error)
         noexcept override;

      virtual void onClose() noexcept override;
      virtual void onCloseError(const boost::system::error_code& error)
         noexcept override;

      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage>
                             message) override;

      virtual void onReceiveError(const boost::system::error_code& error,
                                  std::unique_ptr<daq::asyncmsg::InputMessage>
                                  message) noexcept override;
      virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>
                          message) noexcept override;
      virtual void onSendError(const boost::system::error_code& error,
                               std::unique_ptr<const daq::asyncmsg::OutputMessage>
                               message) noexcept override;

      boost::asio::io_service& m_ioService;
      std::vector<std::unique_ptr<boost::thread> > m_ioServiceThreads;
      std::unique_ptr<boost::asio::io_service::work> m_ioServiceWork;


      std::uint32_t m_transactionId;
      std::uint32_t m_outstanding;
      bool m_printRequest;
      bool m_printData;

      int m_received;
      int m_error;
      uint64_t m_rereqested;
      double m_totalMBytesReceived;
      uint64_t m_fragsWithError;
      std::uint32_t m_maxOutstanding;
      std::uint32_t m_outstandingUnblockThreshold;

      RequestGenerator* m_generator;

      unsigned int m_highestReceived;
      unsigned int m_highestRequested;

      int m_nThreads;

      std::mutex m_tsMutex;
      std::map<std::uint32_t,
               std::chrono::time_point<std::chrono::system_clock> > m_timeStamp;
      std::map<std::uint32_t,std::uint32_t> m_requestedId;

      int m_minRoundTrip;
      int m_maxRoundTrip;
      float m_meanRoundTrip;

      std::mutex m_outstandingMutex;
      std::condition_variable m_condition;

   };



   inline int RequestGenerator::releaseQueueSize() {
      return 0;
   }

   inline bool RequestGenerator::releaseQueueEmpty() {
      return true;
   }



/*******************************************/
inline void RequestGenerator::ebQueuePush(unsigned int lvl1id) {
/*******************************************/
   std::lock_guard<std::mutex> lock(m_ebqMutex);
   m_ebQueue.push(lvl1id);
}

/*******************************************/
inline unsigned int RequestGenerator::ebQueuePop(void) {
/*******************************************/
#ifdef TBB_QUEUE
   int ret;
   m_ebQueue.pop(ret);
#else
   std::lock_guard<std::mutex> lock(m_ebqMutex);
   int ret = m_ebQueue.front();
   m_ebQueue.pop();
#endif
   return ret;
}

/*******************************************/
inline int RequestGenerator::ebQueueSize(void) {
/*******************************************/
   std::lock_guard<std::mutex> lock(m_ebqMutex);
   return m_ebQueue.size();
}

/*******************************************/
inline bool RequestGenerator::ebQueueEmpty(void) {
/*******************************************/
   std::lock_guard<std::mutex> lock(m_ebqMutex);
   return  m_ebQueue.empty();
}



}
#endif
