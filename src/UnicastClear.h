// this is -*- C++ -*-
#ifndef UNICASTCLEAR_H_
#define UNICASTCLEAR_H_


#include <boost/asio/io_service.hpp>  // for io_service
#include <boost/asio/ip/tcp.hpp>      // for tcp, tcp::endpoint
#include <cstddef>                   // for size_t
#include <cstdint>                   // for uint32_t
#include <memory>                     // for shared_ptr
#include <mutex>                      // for mutex
#include <vector>                     // for vector

#include "ROSClear.h"

namespace daq { namespace asyncmsg { class NameService; } }

namespace ROS {
    class ROSSession;

    /**
     * \brief Unicast implementation of ROSClear, using TCP.
     */
   class UnicastClear : public ROSClear {
    public:
        UnicastClear(size_t threshold, boost::asio::io_service& service, daq::asyncmsg::NameService* name_service);
       UnicastClear(size_t threshold, boost::asio::io_service& service, boost::asio::ip::tcp::endpoint*);
        ~UnicastClear();

        virtual void connect() override;
    private:
        /// The implementation of the flush operation.

        virtual void do_flush(uint32_t sequence, std::shared_ptr<std::vector<uint32_t>> events) override;
    private:
        boost::asio::io_service&                     m_service;
        daq::asyncmsg::NameService*                   m_name_service;
      std::vector<std::shared_ptr<ROSSession>>     m_sessions;
      boost::asio::ip::tcp::endpoint* m_endPoint;
    };

    
}

#endif // HLTSV_UNICASTROSCLEAR_H_
