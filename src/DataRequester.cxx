//#define DEBUG_WRAP
#define USE_EFORMAT

#include <thread>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <mutex>
#include <cstdlib>
#include <climits>

#include "ROSTesterSync.h"
#include "is/infodictionary.h"
#include "is/inforeceiver.h"

#include "DataRequester.h"
#include "RosDataInputMsg.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/ClearMsg.h"
#include "ROSasyncmsg/RosDataMsg.h"

#ifdef USE_EFORMAT
# include "eformat/eformat.h"
#endif

#include "ers/ers.h"

using namespace daq;
using namespace ROS;

std::mutex printMutex;
//static std::atomic<bool> waiting=false;

void RequestGenerator::callback(ISCallbackEvent* event) {
   std::string appName=event->name();
   auto pos=appName.find_last_of('.');
   appName=appName.substr(pos+1);

//   std::cout << "Update from " << appName << std::endl; std::cout.flush();

   RequestGenerator* generator=(RequestGenerator*)event->parameter();
   unsigned int index=0;
   for (auto peer : generator->m_otherTesters) {
//      std::cout << "peer=" << peer <<", app name=" << appName << ", index=" << index <<std::endl;
      if (appName==peer) {
         ROSTesterSync syncInfo;
         generator->m_dictionary.getValue(event->name(),syncInfo);
         generator->m_otherSync[index]=syncInfo.syncPoint;
         generator->m_condition.notify_all();
         break;
      }
      index++;
   }
}

RequestGenerator::RequestGenerator(std::vector<unsigned int>& channelList,
                                   std::vector<uint32_t>& roiDistribution,
                                   float l2Percent, float ebPercent, float metPercent,
                                   int deleteGrouping,
                                   uint32_t l1Rate, unsigned int maxL1,
                                   unsigned int eventsPerEcr,
                                   unsigned int maxOutstanding,
                                   bool syncWithIS,
                                   bool preloaded,
                                   std::string partitionName,
                                   std::vector<std::string>& otherTesters,
                                   std::string myName,
                                   std::shared_ptr<ROSClear> clearSession)
   : TriggerGenerator(channelList,roiDistribution,
                      deleteGrouping,l2Percent,ebPercent,maxL1,eventsPerEcr),
     m_clearSession(clearSession),
     m_nextRequester(0),
     m_l2Generated(0),
     m_ebGenerated(0),
     m_ebTransactionId(0x80000000),
     m_l1Rate(l1Rate),
     m_blockMargin(maxOutstanding/ebPercent*100),
     m_highestReceived(0),
     m_wrapping(false),
     m_wrapCount(0),
     m_maxL1(maxL1),
     m_initialL1(0),
     m_syncWithIS(syncWithIS),
     m_preloaded(preloaded),
     m_channelList(channelList) {
   if (metPercent>0) {
      m_metThreshold=100.0/metPercent;
   }
   else {
      m_metThreshold=0;
   }
   m_syncDeletesWithEBRequests=false;

   for (unsigned int index=0;index<otherTesters.size(); index++) {
      m_otherSync.push_back(0);
   }
   m_syncPoint=0;
   m_lastSyncPoint=0;

   m_isInfoName="DF.ROSTesterSync."+myName;

   if (m_syncWithIS) {
      auto ipcPartition=IPCPartition(partitionName.c_str());
      m_dictionary=ISInfoDictionary(ipcPartition);
      m_otherTesters=otherTesters;

      ROSTesterSync sync;
      sync.syncPoint=m_syncPoint;
      try {
         m_dictionary.update(m_isInfoName,sync);
      }
      catch (daq::is::InfoNotFound& exc1) {
         try {
            m_dictionary.insert(m_isInfoName,sync);
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      catch (daq::is::Exception& exc) {
         // Issue a warning and carry on
         ers::warning(exc);
      }

      m_infoReceiver=new ISInfoReceiver(partitionName);
   }

   m_intervalStart=std::chrono::system_clock::now();
   m_l1Interval=0;

}


RequestGenerator::~RequestGenerator() {
   std::cout << "RequestGenerator destructor:\n";
   if (m_syncWithIS) {
      for (unsigned int index=0;index<m_otherTesters.size();index++) {
         std::cout << "unsubscribing DF.ROSTesterSync."+m_otherTesters[index] << std::endl;
         try {
            m_infoReceiver->unsubscribe(
               "DF.ROSTesterSync."+m_otherTesters[index]);
            std::cout << "Succesfully unsubscribed DF.ROSTesterSync."+m_otherTesters[index] << std::endl;
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      m_dictionary.remove(m_isInfoName);
      delete m_infoReceiver;
   }
}


void RequestGenerator::setInstance(int instance, int totalInstances) {
   m_instance=instance;
   resetL1();
   m_totalInstances=totalInstances;
   if (m_useList) {
      m_blockMargin=0x4000000;
   }
   //m_blockMargin/=totalInstances;
   set_level1Id(m_instance);
   m_initialL1=m_level1Id;
   m_highestReceived=m_level1Id;
   std::cout << "Level 1 Id=" << std::hex << m_level1Id << std::dec << std::endl;
   if (m_syncWithIS) {
#if 0
      for (unsigned int index=0;index<m_otherTesters.size();index++) {
         m_infoReceiver->subscribe("DF.ROSTesterSync."+m_otherTesters[index],
                                   [this, index] (ISCallbackEvent* event) {
                                      ROSTesterSync syncInfo;
                                      m_dictionary.getValue(event->name(),syncInfo);
                                      m_otherSync[index]=syncInfo.syncPoint;
                                      m_condition.notify_all();
                                   }, 0);
   }
#else
      for (unsigned int index=0;index<m_otherTesters.size();index++) {
         try {
//            std::cout << "DataRequester subscribing to " << "DF.ROSTesterSync."+m_otherTesters[index] << std::endl;
            m_infoReceiver->subscribe("DF.ROSTesterSync."+m_otherTesters[index],
                                      callback, this);
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
#endif
   }
}


uint64_t RequestGenerator::l2RequestsGenerated() {
   return m_l2Generated;
}
uint64_t RequestGenerator::ebRequestsGenerated() {
   return m_ebGenerated;
}

uint32_t RequestGenerator::wrapCount() {
   return m_wrapCount;
}

void RequestGenerator::registerRequester(DataRequester* requester) {
   m_requester.push_back(requester);
}




/*******************************************/
unsigned int RequestGenerator::releaseQueuePop(void) {
/*******************************************/
//    std::lock_guard<std::mutex> lock(m_relqMutex);
//    int ret = m_releaseQueue.front();
//    m_releaseQueue.pop();
//    return ret;
   return 0;
}

bool RequestGenerator::condition() {

   m_l1Interval+=m_totalInstances;
   //std::cout << "RequestGenerator::condition()  m_l1Interval=" << m_l1Interval << std::endl;
   if (m_l1Interval>m_l1Rate*10.5) {
      auto elapsed=std::chrono::system_clock::now()-m_intervalStart;
      auto milliSeconds=std::chrono::duration_cast<std::chrono::milliseconds> (elapsed).count();
      if (milliSeconds<10) {
         milliSeconds=std::chrono::duration_cast<std::chrono::milliseconds> (elapsed).count();
         std::this_thread::sleep_for(std::chrono::milliseconds(10-milliSeconds));
      }
      m_intervalStart=std::chrono::system_clock::now();
      m_l1Interval=0;
   }
   if (m_termFlag) {
      return false;
   }
   else {
      return true;
   }
}

void RequestGenerator::stop() {
   m_termFlag=true;

   m_condition.notify_all();  // In case we're waiting for a condition somewhere
}

void RequestGenerator::start() {
   m_termFlag=false;
}

void RequestGenerator::printSet(std::set<unsigned int>& data) {
   if (data.size()==0) {
      std::cout << "EMPTY";
   }
   else {
      int nPrinted=0;
      for (auto id : data) {
         std::cout << " " << id;
         nPrinted++;
         if (nPrinted%16==0) {
            std::cout << std::endl;
         }
      }
   }
   std::cout << std::dec << std::endl;
}

void RequestGenerator::dump() {
   std::cout << "RequestGenerator::" 
             << " highest received l1: " << std::hex << m_highestReceived
             << ". Wrapping: " << m_wrapping
             << ", wrap count: " << m_wrapCount
             << ", syncPoint: " << m_syncPoint
             << std::endl;

   std::cout << "Outstanding requests: " << std::hex;
   printSet(m_outstandingRequest);

   std::cout << "Pending release: " << std::hex;
   printSet(m_pendingRelease);

   std::cout << "Wrapped pending release: " << std::hex;
   printSet(m_wrappedPending);

}

void RequestGenerator::l2Request(const unsigned int level1Id,
                              const std::vector<unsigned int> * rols,
                              const unsigned int /*destination*/,
                              const unsigned int transactionId){
   std::unique_ptr<DataRequestMsg> requestMsg
      (new DataRequestMsg(4,transactionId));

   requestMsg->level1Id(level1Id);

   if (m_metThreshold!=0 && m_l2Generated%m_metThreshold==0) {
            std::cout << "MEt request\n";
      requestMsg->addRol(0x7d1234);
   }
   else {
      for (unsigned int chan=0;chan<rols->size();chan++) {
         requestMsg->addRol(rols->at(chan));
      }
   }
   m_l2Generated++;
   //std::cout << "Sending RoI request for level1Id " << level1Id << std::endl;

   m_requester[m_nextRequester++]->sendRequest(std::move(requestMsg));
   if (m_nextRequester==m_requester.size()) {
      m_nextRequester=0;
   }
}

std::vector<unsigned int>* RequestGenerator::channels() {
   return &m_channelList;
}
void RequestGenerator::ebRequest(const unsigned int level1Id,
                              const unsigned int /*destination*/) {

   if (!m_wrapping && abs((long long)level1Id-m_highestReceived)>0x80000000) {
//#ifdef DEBUG_WRAP
      {
      std::lock_guard<std::mutex> lock(printMutex);
      std::cout << "ebRequest: wrap detected at l1=" << std::hex << level1Id << ", highest received=" << m_highestReceived
                << " abs diff=" << abs((long long)level1Id-m_highestReceived) << std::dec << std::endl;
      }
//#endif
      // Wrapped
      m_wrapCount++;
      m_wrapping=true;
      uint64_t syncHi=m_wrapCount;
      m_syncPoint=(syncHi<<32)|(m_syncPoint&0xffffffff);
   }
   if (m_maxL1-level1Id<0x8000) {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_outstandingRequest.insert(level1Id);
   }

   std::unique_ptr<DataRequestMsg> requestMsg(
      new DataRequestMsg(m_channelList,level1Id,m_ebTransactionId++));

   //std::cout << "Sending EB request for level1Id " << level1Id << std::endl;
   m_requester[m_nextRequester++]->sendRequest(std::move(requestMsg));
   if (m_nextRequester==m_requester.size()) {
      m_nextRequester=0;
   }

   m_ebGenerated++;
}

void RequestGenerator::receivedL2Fragment(unsigned int receivedId) {
   ebQueuePush(receivedId);
}

bool RequestGenerator::inCurrentRange(unsigned int level1Id) {
   long long difference=(long long)level1Id-(long long)m_highestReceived;
   bool ok=(abs(difference)<0x80000 || (m_wrapping && level1Id<8192));
   if (!ok) {
      std::cout << std::hex << level1Id << " out of range, highest received="
                << m_highestReceived
                << std::dec
                << ", difference=" << difference << "(abs=" << abs(difference) << ")"
                << ", wrapping=" << m_wrapping
 << std::endl;
   }
   return ok;
}

void RequestGenerator::processPendingRelease(unsigned int level1Id) {
   m_pendingRelease.insert(level1Id);

   auto iter=m_pendingRelease.begin();
   while (iter!=m_pendingRelease.end() &&
#ifdef DEBUG_WRAP
      ((*iter)&0x0fffffff)<=level1Id
#else
      *iter<=level1Id
#endif
      ) {
      m_clearSession->add_event(*iter++);
   }
   m_pendingRelease.erase(m_pendingRelease.begin(),iter);
}

void RequestGenerator::receivedEBFragment(unsigned int level1Id) {
   std::lock_guard<std::mutex> lock(m_mutex);
   if (m_maxL1-level1Id<0x8000) {
      auto oIter=m_outstandingRequest.find(level1Id);
      if (oIter!=m_outstandingRequest.end()) {
         m_outstandingRequest.erase(oIter);
      }
      else {
         std::cout << "WTF why isn't " << std::hex << level1Id << std::dec << " in set of outstanding requests\n";
      }
   }

   if (m_eventsPerEcr) {
      unsigned char lastEcr=m_highestReceived>>24;
      unsigned char currentEcr=level1Id>>24;
      if ((currentEcr>=lastEcr && currentEcr<254 && level1Id>m_highestReceived) ||
          (lastEcr>250 && currentEcr<8)) {
         m_highestReceived=level1Id;
         m_condition.notify_all();
      }
      // else if (waiting) {
      //    std::cout << std::hex << "receivedEBFragment level1Id=" << level1Id
      //              << ", highest received=" << m_highestReceived << std::dec
      //              << ", currentEcr=" << (int)currentEcr
      //              << ", lastEcr=" << (int)lastEcr <<std::endl;
      //    std::cout.flush();
      // }
   }
   else {
      int receivedDifference=level1Id-m_highestReceived;

      if (m_highestReceived==0x7fffffff) {
         std::cout << std::hex << "m_highestReceived=" << m_highestReceived
                   << " level1Id=" << level1Id
                   << " receivedDifference=" << receivedDifference
                   << std::dec << std::endl;
      }

      if (receivedDifference>0 && receivedDifference<8192) {
         m_highestReceived=level1Id;
         m_condition.notify_all();
      }
   }
   if (m_wrapping) {

      if  (m_highestReceived-level1Id>level1Id) {
         m_wrappedPending.insert(level1Id);
      }
      else {
         processPendingRelease(level1Id);
      }

//      if (level1Id>1024 && (m_highestReceived-level1Id>1024)) {
      unsigned int highestOutstandingRequest=level1Id;
      auto endIter=m_outstandingRequest.rbegin();
      if (endIter!=m_outstandingRequest.rend()) {
         highestOutstandingRequest=*endIter;
      }
#ifdef DEBUG_WRAP
      std::cout << std::hex << "Received " << level1Id
                << ", highest outstanding=" << highestOutstandingRequest
                << std::dec << std::endl;
#endif

      if (highestOutstandingRequest<0x8000) {
#ifdef DEBUG_WRAP
         std::cout << "Wrap complete, l1=" << level1Id
                   << ", highest outstanding=" << highestOutstandingRequest
                   << ", diff=" << highestOutstandingRequest-level1Id
                   << ", highest received=" << m_highestReceived
                   << std::endl;

         std::cout << "Pending release:";
         for (auto pIter : m_pendingRelease) {
            std::cout << " " << pIter;
         }
         std::cout << std::endl;

         for (auto req : m_requester) {
            req->debug();
         }
         std::cout << std::endl;
#endif
         auto lastIter=m_pendingRelease.rbegin();
         if (lastIter!=m_pendingRelease.rend()) {
            processPendingRelease(*lastIter);
         }

         m_pendingRelease.swap(m_wrappedPending);
         lastIter=m_pendingRelease.rbegin();
         if (lastIter!=m_pendingRelease.rend()) {
            m_highestReceived=*lastIter;
//               std::cout << "Processing wrapped pending up to " << highestOutstandingRequest << std::endl;
            processPendingRelease(m_highestReceived);
         }
         else {
            m_highestReceived=m_initialL1;
         }
         m_wrapping=false;
      m_condition.notify_all();
      }
#ifdef DEBUG_WRAP
      else {
         std::cout << "Still wrapping l1=" << level1Id  << std::endl;
      }
#endif
   }
   else {
      processPendingRelease(level1Id);
   }
}

void RequestGenerator::dropEBRequest(unsigned int level1Id) {
   if (m_maxL1-level1Id<0x8000) {
      std::lock_guard<std::mutex> lock(m_mutex);
      std::cout << "Dropping request for " << std::hex << level1Id << std::dec << std::endl;
      auto oIter=m_outstandingRequest.find(level1Id);
      if (oIter!=m_outstandingRequest.end()) {
         m_outstandingRequest.erase(oIter);
      }
      else {
         std::cout << "WTF why isn't " << std::hex << level1Id << std::dec << " in set of outstanding requests\n";
      }
   }
}

void RequestGenerator::releaseQueuePush(unsigned int level1Id) {
   std::lock_guard<std::mutex> lock(m_mutex);

   if (!m_wrapping && abs((long long)level1Id-m_highestReceived)>0x80000000) {
//#ifdef DEBUG_WRAP
      std::cout << "releaseQueuePush: wrap detected at l1=" << std::hex
                << level1Id << ", highest received=" << m_highestReceived
                << std::dec << std::endl;
//#endif
      // Wrapped
      m_wrapCount++;
      m_wrapping=true;
      uint64_t syncHi=m_wrapCount;
      m_syncPoint=(syncHi<<32)|(m_syncPoint&0xffffffff);
   }
   if (level1Id < m_highestReceived && !m_wrapping) {
      m_clearSession->add_event(level1Id
#ifdef DEBUG_WRAP
                                |0x80000000
#endif
         );
   }
   else if (m_wrapping && (level1Id < 8192) ) {
      m_wrappedPending.insert(level1Id
#ifdef DEBUG_WRAP
                              |0x40000000
#endif
         );
   }
   else {
      m_pendingRelease.insert(level1Id
#ifdef DEBUG_WRAP
                              |0x20000000
#endif
         );
   }
}

void RequestGenerator::syncL1(uint32_t level1Id) {
   if (m_syncWithIS) {
      const unsigned int syncInterval=0x4000;
      m_syncPoint=(m_syncPoint&0xffffffff00000000)|level1Id;
      if (m_syncPoint-m_lastSyncPoint>syncInterval) {
//      std::cout << "Updating sync in IS\n";std::cout.flush();
         ROSTesterSync sync;
         sync.syncPoint=m_syncPoint;
         try {
            m_dictionary.update(m_isInfoName,sync);
         }
         catch (daq::is::InfoNotFound& exc1) {
            try {
               m_dictionary.insert(m_isInfoName,sync);
            }
            catch (daq::is::Exception& exc) {
               // Issue a warning and carry on
               ers::warning(exc);
            }
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
         m_lastSyncPoint=m_syncPoint;

         for (unsigned int peer=0;peer<m_otherSync.size();peer++) {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (m_otherSync[peer]<m_syncPoint-syncInterval) {
//             std::cout << "Waiting for peer sync syncPoint="
//                       <<m_syncPoint << ",  peer syncPoint="
//                       << m_otherSync[peer] << std::endl;std::cout.flush();
               m_condition.wait(lock,
                                [this,peer]{
                                   return (m_otherSync[peer]>m_syncPoint-500 || m_termFlag);
                                });
               if (m_termFlag) {
                  return;
               }
            }
         }
      }
   }

   if (!m_preloaded && m_highestReceived>1024) {
      if (m_eventsPerEcr) {
         char latestEcrReceived=m_highestReceived>>24;
         char currentEcr=level1Id>>24;
         char ecrDiff=currentEcr-latestEcrReceived;
         if (ecrDiff<-4 || ecrDiff>4) {
            std::unique_lock<std::mutex> lock(m_mutex);
            std::cout << "Waiting for L1 sync\n";
            // waiting=true;
            m_condition.wait(lock,
                             [level1Id,currentEcr, this]{
                                char ecrDiff=currentEcr-(m_highestReceived>>24);
                                std::cout << std::hex << "l1Id=" << level1Id << ", m_highestReceived=" <<m_highestReceived << std::dec << ", currentEcr=" << (int)currentEcr << ", ecrDiff=" << (int)ecrDiff << std::endl;
                                std::cout.flush();
                                return ((ecrDiff<4 && ecrDiff>-4) || m_termFlag);
                          });
            // waiting=false;
         }
      }
      else if (level1Id>m_highestReceived+m_blockMargin) {
         std::unique_lock<std::mutex> lock(m_mutex);
         //std::cout << "Waiting for L1 sync\n";
         m_condition.wait(lock,
                          [level1Id, this]{
                             return (level1Id<m_highestReceived+m_blockMargin || m_termFlag);
                          });
      }
   }
}


void RequestGenerator::releaseRequest(const std::vector<unsigned int>* level1Ids,
                                   const unsigned int /*oldestL1id*/) {
   for (auto l1 : *level1Ids) {
      m_clearSession->add_event(l1);
   }
}

DataRequester::DataRequester(boost::asio::io_service& ioService,
                             bool printReq, bool printData,
                             int nThreads,
                             int maxOutstanding, int outstandingUnblock,
                             RequestGenerator* generator) :
   daq::asyncmsg::Session(ioService),
   m_ioService(ioService),
   m_transactionId(0x80000000),
   m_outstanding(0),
   m_printRequest(printReq),
   m_printData(printData),
   m_received(0),
   m_error(0),
   m_rereqested(0),
   m_totalMBytesReceived(0.0),
   m_fragsWithError(0),
   m_maxOutstanding(maxOutstanding),
   m_outstandingUnblockThreshold(outstandingUnblock),
   m_generator(generator),
   m_highestReceived(0),
   m_nThreads(nThreads),
   m_minRoundTrip(INT_MAX),
   m_maxRoundTrip(-1),
   m_meanRoundTrip(0.0){

   generator->registerRequester(this);
}

DataRequester::~DataRequester(){
    std::cout << "DataRequester destructor:  outstanding=" << m_outstanding
              << ", received=" << m_received
              << ", errors=" << m_error
              << ", rerequested=" << m_rereqested
#ifdef TIME_REQUESTS
              << ", round trip time: min=" << m_minRoundTrip
              << ",max=" << m_maxRoundTrip
#endif
              << std::endl;
}

void DataRequester::debug() {
   std::cout << "Requests outstanding: " <<std::hex;
   for (auto reqIter : m_requestedId) {
      std::cout << "  "<< reqIter.second;
   }
   std::cout <<std::dec << std::endl;
}

void DataRequester::connect(boost::asio::ip::tcp::endpoint endpoint,
                            const std::string& myName) {
   m_ioServiceWork.reset(new boost::asio::io_service::work(m_ioService));
   std::cout << "DataRequester::connect name=" << myName
             << ", endpoint=" << endpoint << ", nThreads=" << m_nThreads << std::endl;
   for (int threadCount=0; threadCount<m_nThreads; threadCount++) {
      std::unique_ptr<boost::thread> thread(
         new boost::thread(boost::bind(&boost::asio::io_service::run, &m_ioService)));
      pthread_setname_np(thread->native_handle(),"RequesterThread");

      m_ioServiceThreads.push_back(std::unique_ptr<boost::thread>(thread.release()));
   }

   asyncOpen(myName, endpoint);

   while(state() != daq::asyncmsg::Session::State::OPEN) {
      usleep(10000);
   }

}

void DataRequester::disconnect(){
//   std::cout << "DataRequester::disconnect()\n";
   m_ioServiceWork.reset(); // This should tell io_service to stop!
   asyncClose();

   for (unsigned int thread=0; thread< m_ioServiceThreads.size(); thread++) {
      m_ioServiceThreads[thread]->join();
   }

   m_ioService.stop();
   m_ioService.reset();

    std::cout << "DataRequester disconnect:  outstanding=" << m_outstanding
              << ", received=" << m_received
              << ", errors=" << m_error
#ifdef TIME_REQUESTS
              << ", round trip time: min=" << m_minRoundTrip
              << ", max=" << m_maxRoundTrip
              << ", mean=" << m_meanRoundTrip
#endif
              << std::endl;
}

uint32_t DataRequester::outstanding() {
   return m_outstanding;
}

double DataRequester::megabytesReceived() {
   return m_totalMBytesReceived;
}

uint64_t DataRequester::fragmentsWithError() {
   return m_fragsWithError;
}

uint64_t DataRequester::reRequests() {
   return m_rereqested;
}

void DataRequester::onOpen() noexcept{

}

void DataRequester::onOpenError(const boost::system::error_code& error) noexcept{
  ERS_LOG("Error opening session "
          << error.message() << " (" << error << "). Aborting\n");
}


void DataRequester::onClose() noexcept{

}

void DataRequester::onCloseError(const boost::system::error_code& error) noexcept{
  ERS_LOG("Error closing session with peer " << remoteEndpoint() << ": " <<
      error.message() << " (" << error << "). Aborting\n");
}


std::unique_ptr<asyncmsg::InputMessage> DataRequester::createMessage(std::uint32_t typeId,
                                                           std::uint32_t transactionId,
                                                           std::uint32_t size) noexcept{

   //std::cout << std::hex << "DataRequester::createMessage(" << typeId <<","<<transactionId<<","<<size<<")\n" << std::dec;
   if (typeId == RosDataMsg::TYPE_ID) {
      std::unique_ptr<asyncmsg::InputMessage> message;
      try {
         message.reset(new RosDataInputMsg(size,transactionId));
      }
      catch (std::exception& ex) {
         ERS_LOG("Unexpected message size " << size << " from " << remoteEndpoint() << ". Ignoring.");
         return std::unique_ptr<asyncmsg::InputMessage>();
      }
      return message;
   }
   else {
      ERS_LOG("Unexpected message type " << typeId << " from " << remoteEndpoint() << ". Ignoring.");
      return std::unique_ptr<asyncmsg::InputMessage>();
   }
}


void DataRequester::onReceive(std::unique_ptr<asyncmsg::InputMessage> message) {
   //std::cout << "Received message " << std::hex << message->transactionId() << std::dec << std::endl;
   std::unique_ptr<RosDataInputMsg> dataMsg(dynamic_cast<RosDataInputMsg*>(message.release()));
   if (!dataMsg) {
      std::cerr << "Could not cast input message to RosDataInputMsg\n";
      return;
   }
   if (dataMsg->size()==0) {
      std::cerr << "Received 0 sized RosDataInputMsg\n";
      return;
   }

   unsigned int requestedLevel1Id=0xffffffff;
#ifdef TIME_REQUESTS
   int usec;
#endif
   {
      std::lock_guard<std::mutex> lock(m_tsMutex);
      m_received++;
      m_totalMBytesReceived+=(float) dataMsg->size()/1e6;
#ifdef TIME_REQUESTS
      auto tsIter=m_timeStamp.find(dataMsg->transactionId());
      auto elapsed=std::chrono::system_clock::now()-tsIter->second;
      m_timeStamp.erase(tsIter);
      usec=std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count();
      m_meanRoundTrip+=((float) usec-m_meanRoundTrip)/(float) m_received;
      //std::cout << "complete=" << m_received << ", usec=" << usec << ", mean=" << m_meanRoundTrip << std::endl;

#endif
      std::map<std::uint32_t,std::uint32_t>::iterator idIter=m_requestedId.find(dataMsg->transactionId());
      if (idIter != m_requestedId.end()) {
         requestedLevel1Id=idIter->second;
         m_requestedId.erase(idIter);

//          if (m_highestReceived<requestedLevel1Id){
//             m_highestReceived=requestedLevel1Id;
//          }
      }
      else {
         std::cout << "Can't find level1id for xid "
                   << std::hex << dataMsg->transactionId() << std::endl;
      }
   }
#ifdef TIME_REQUESTS
   if (usec<m_minRoundTrip) {
      m_minRoundTrip=usec;
   }
   if (usec>m_maxRoundTrip) {
      m_maxRoundTrip=usec;
   }
#endif

#ifdef USE_EFORMAT
//   const eformat::ROBFragment<const unsigned int*> fragment(dataMsg->m_fragments.data());
   const eformat::ROBFragment<const unsigned int*>* fragment=
      new eformat::ROBFragment<const unsigned int*>(dataMsg->m_fragments);
#endif

   if (m_printData) {
      std::lock_guard<std::mutex> lock(printMutex);
#ifdef TIME_REQUESTS
      std::cout << "Elapsed time "
                << usec << "us";// << std::endl;
#endif
      std::cout << std::setfill('0') << std::hex
                << " ostndng=" << std::setw(2) << m_outstanding
                << " xid=" << std::setw(8) << dataMsg->transactionId()
                << " l1Id=" << std::setw(8) << requestedLevel1Id
                << " size=" << std::setw(4) << dataMsg->size();// << std::endl;

      std::cout << " data:";
      for (int word=0; word<16; word++) {
         std::cout << " " << std::setw(8) << dataMsg->m_fragments[word];
      }
      std::cout << std::dec << std::setfill(' ') << std::endl;

   }

   // Check the status of all ROB fragments in the packet
   unsigned int allStatus=0;
   unsigned int rol=0;
#ifdef USE_EFORMAT
   unsigned int offset=0;
   do {
       if (!fragment->check_rob_noex()) {
         std::lock_guard<std::mutex> lock(printMutex);
          std::cout << "Problem with ROB fragment for L1 id " << std::hex << requestedLevel1Id << std::dec << std::endl;
          std::vector<eformat::FragmentProblem> probs;
          fragment->rob_problems(probs);
          for (auto p : probs) {
             std::cout << " ROB problem=" << p << std::endl;
          }
          fragment->rod_problems(probs);
          for (auto p : probs) {
             std::cout << " ROD problem=" << p << std::endl;
          }
       }
      unsigned int receivedId=fragment->rod_lvl1_id();
      if (receivedId != requestedLevel1Id) {
         std::lock_guard<std::mutex> lock(printMutex);
         std::cout << "Received level1 Id " << std::hex << receivedId
                   << " from rol " << rol
                   << " ROB source id " << fragment->rob_source_id()
                   << " expected " << requestedLevel1Id
                   << std::dec << std::endl;
      }
      auto status=*fragment->status() & 0xffff0000;
      if (m_printData) {
         std::lock_guard<std::mutex> lock(printMutex);
         std::cout << std::hex
                   << "ROL " << rol
                   << " ROB source id " << fragment->rob_source_id()
                   << " fragment status " << status
                   << std::dec << std::endl;
      }


      allStatus|=status;
      rol++;
      if (offset<dataMsg->size()) {
         int fragSize=fragment->fragment_size_word()*4;
         if (fragSize) {
            offset+=fragSize;
            auto newfragment=
               new eformat::ROBFragment<const unsigned int*>(fragment->end());
            delete fragment;
            fragment=newfragment;
         }
         else {
            std::cout << "Bad fragment, size 0\n";
            offset=dataMsg->size();
         }
      }
   } while (offset<dataMsg->size());
   delete fragment;
#else
   unsigned int offset=0;
   unsigned int messageSize=dataMsg->size()/sizeof(unsigned int);
   const unsigned int l1Offset=5;
   do {
      unsigned int robFragSize=dataMsg->m_fragments[offset+1];
      unsigned int rodFragOffset=offset+dataMsg->m_fragments[offset+2];

      if (dataMsg->m_fragments[rodFragOffset]!=0xee1234ee) {
         std::cout << "Bad ROD header marker "
                   << std::hex << dataMsg->m_fragments[rodFragOffset]
                   << std::dec << std::endl;
      }

      unsigned int receivedId=dataMsg->m_fragments[rodFragOffset+l1Offset];
      if (receivedId != requestedLevel1Id) {
         std::cout << "Received level1 Id " << receivedId
                   << " from rol " << rol
                   << " ROB source id " << dataMsg->m_fragments[offset+4]
                   << " expected " << requestedLevel1Id
                   << std::endl;
      }

      auto status=dataMsg->m_fragments[offset+6] & 0xffff0000;
      if (m_printData) {
         std::lock_guard<std::mutex> lock(printMutex);
         std::cout << std::hex
                   << "ROL " << rol
                   << " ROB source id " << dataMsg->m_fragments[offset+4]
                   << " ROD source id " << dataMsg->m_fragments[rodFragOffset+3]
                   << " fragment status " << status << std::dec << std::endl;
      }
      allStatus|=status;
      rol++;
      offset+=robFragSize;
   } while (offset<messageSize);
#endif

   if (allStatus) {
      m_fragsWithError++;
      if (m_printData) {
         std::lock_guard<std::mutex> lock(printMutex);
         std::cout << "Bad fragment status " << std::hex << allStatus  
                   << " from l1 id " << requestedLevel1Id << std::dec
                   << " (" << requestedLevel1Id << ")" << std:: endl;
      }
   }

   bool reRequest=false;
   if ((dataMsg->transactionId()&0x80000000)==0) {
      m_generator->receivedL2Fragment(requestedLevel1Id);
   }
   else {
      if ((allStatus&0x40000000) != 0) {
         if ((m_generator->inCurrentRange(requestedLevel1Id))
             && m_generator->condition()) {
            reRequest=true;
            std::unique_ptr<DataRequestMsg> requestMsg(
               new DataRequestMsg(*m_generator->channels(),requestedLevel1Id,dataMsg->transactionId()));

            //std::cout << "Resending EB request for level1Id " << requestedLevel1Id << std::endl;
            std::lock_guard<std::mutex> lock(m_tsMutex);
#ifdef TIME_REQUESTS
            m_timeStamp[requestMsg->transactionId()]=std::chrono::system_clock::now();
#endif
            m_requestedId[requestMsg->transactionId()]=requestedLevel1Id;
            m_rereqested++;
            asyncSend(std::move(requestMsg));
         }
         else {
            m_generator->dropEBRequest(requestedLevel1Id);
         }
      }
      else {
         m_generator->receivedEBFragment(requestedLevel1Id);
      }
   }

   if (!reRequest) {
      std::unique_lock<std::mutex> lock(m_outstandingMutex);
      m_outstanding--;
//         std::cout << "onReceive: " << m_outstanding << " requests outstanding";
      if (m_outstanding<=m_outstandingUnblockThreshold) {
//         std::cout << ", signalling condition\n";
         m_condition.notify_all();
      }
   }
}


void DataRequester::onReceiveError(const boost::system::error_code& error,
                                   std::unique_ptr<asyncmsg::InputMessage> message) noexcept{
   m_error++;
   if (state()!=daq::asyncmsg::Session::State::OPEN) {
      std::cout << "Closing\n";
   }
   else if (error) {
      if (error!=boost::asio::error::eof && error!=boost::asio::error::operation_aborted) {
         if (message) {
            ERS_LOG("Error receiving message type " << message->typeId()
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");
         }
         else {
            ERS_LOG("Error receiving message -- message not set -- "
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");

         }
         asyncClose();
      }
//       else {
//          std::cout << remoteEndpoint() << " shut connection\n";
//       }
   }
}

void DataRequester::onSend(std::unique_ptr<const asyncmsg::OutputMessage> message) noexcept{
   std::unique_ptr<const DataRequestMsg> requestMsg(dynamic_cast<const DataRequestMsg*>(message.release()));
   if (requestMsg) {
      if (m_printRequest) {
         std::cout << "Sent request message for " << requestMsg->nRequestedRols() << " ROLs:" << std::hex;
//       std::unique_ptr<std::vector<std::uint32_t> > rols=requestMsg->requestedRols();
//       for (auto rolId : *rols) {
//          std::cout << " " << rolId;
//       }
//       std::cout << std::endl << std::dec;
      }
//       std::lock_guard<std::mutex> lock(m_tsMutex);
//       auto xid=requestMsg->transactionId();
//      m_timeStamp[xid]=std::chrono::system_clock::now();
      asyncReceive(); // we want to get a response!
   }
}

void DataRequester::onSendError(const boost::system::error_code& error, std::unique_ptr<const asyncmsg::OutputMessage> message) noexcept{
   ERS_LOG("Error sending message type " << message->typeId() << " to " << remoteEndpoint() << ": " <<
             error.message() << " (" << error << ". Aborting.\n");
   asyncClose();
}


// ===========================================================================
void DataRequester::sendRequest(std::unique_ptr<DataRequestMsg> requestMsg) {
// ===========================================================================
   {
      std::unique_lock<std::mutex> lock(m_outstandingMutex);
//      std::cout << m_outstanding << " requests outstanding, ";
      if (m_outstanding >= m_maxOutstanding) {
//         std::cout << "waiting for outstanding to fall below " << m_outstandingUnblockThreshold << std::endl; std::cout.flush();
         m_condition.wait(lock,
                          [this]{return (m_outstanding<m_maxOutstanding);});
      }
      else {
//         std::cout << ", OK\n";
      }
      m_outstanding++;
   }

   unsigned int level1Id=requestMsg->level1Id();
   m_generator->syncL1(level1Id);
   {
      std::lock_guard<std::mutex> lock(m_tsMutex);
#ifdef TIME_REQUESTS
      m_timeStamp[requestMsg->transactionId()]=std::chrono::system_clock::now();
#endif
      m_requestedId[requestMsg->transactionId()]=level1Id;
   }
   asyncSend(std::move(requestMsg));
}


