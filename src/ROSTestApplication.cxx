#include <algorithm>
#include <boost/program_options.hpp>
#include <iostream>
#include <chrono>
#include <random>
#include <thread>
#include <cstdlib>

#include "DataRequester.h"
//#include "Clearer.h"

#include "UnicastClear.h"
#include "MulticastROSClear.h"

#include "ers/ers.h"

#include "ipc/core.h"

#include "is/infodictionary.h"
#include "is/exceptions.h"
#include "ROSTesterInfo.h"

#include "config/Configuration.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "dal/Component.h"
#include "dal/Detector.h"

#include "DFdal/ROSTester.h"

#include "DFdal/ROS.h"
#include "DFdal/ReadoutConfiguration.h"
#include "DFdal/ReadoutModule.h"
#include "DFdal/InputChannel.h"
#include "DFdal/DFParameters.h"
#include "DFdal/DataFile.h"
#include "asyncmsg/NameService.h"

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/FSM/FSMCommands.h"

#include "EventStorage/pickDataReader.h"
#include "eformat/eformat.h"

using namespace ROS;

namespace po = boost::program_options;


class TheTest : public daq::rc::Controllable {
public:
   TheTest(int argc, char* argv[]);
   ~TheTest() noexcept;

   void configure(const daq::rc::TransitionCmd&) final;
   void connect(const daq::rc::TransitionCmd&) final;
   void disconnect(const daq::rc::TransitionCmd&) final;
   void unconfigure(const daq::rc::TransitionCmd&) final;
   void prepareForRun(const daq::rc::TransitionCmd&) final;
   void stopHLT(const daq::rc::TransitionCmd&) final;
   void user(const daq::rc::UserCmd& command) final;
   void publish() final;
   void run();
   void uncontrolledRun();
   bool controlled();
private:
   void resetStatistics();

   po::variables_map m_config;
   std::string m_executable;
   std::string m_name;
   std::string m_rosName;
   RequestGenerator* m_generator;
   daq::asyncmsg::NameService* m_nameService;
   std::vector<std::shared_ptr<DataRequester> > m_requester;
   std::thread* m_thread;
   bool m_active;
   bool m_interactive;
   uint32_t m_nSessions;
   uint32_t m_startDelay;

   boost::asio::io_service* m_clearIOService;
   std::shared_ptr<ROSClear> m_clearSession;
   std::unique_ptr<boost::asio::io_service::work> m_clearWork;
   std::unique_ptr<boost::thread> m_clearThread;

   std::string m_address;
   std::string m_mcAddress;
   std::string m_proto;
   uint32_t m_port;

   float m_ebPercent;
   float m_l2Percent;
   float m_metPercent;

   bool m_printReq;
   bool m_printData;

   uint32_t m_nThreads;
   uint32_t m_instance;
   uint32_t m_l1Rate;
   uint32_t m_totalInstances;
   uint32_t m_nRequests;
   uint32_t m_deleteGrouping;
   uint32_t m_maxOutstanding;
   uint32_t m_initialL1;
   double m_mbytesReceivedRunLast;
   double m_mbytesReceivedISLast;
   uint64_t m_numGeneratedLast;
   std::vector<unsigned int> m_channelList;
   std::vector<uint32_t> m_roiDistribution{90,8,2};

   boost::asio::ip::tcp::endpoint m_endPoint;
//   std::unique_ptr<std::vector<boost::asio::io_service>>
   boost::asio::io_service* m_ioService;
   int m_generateMode;
   bool m_uncontrolled;
   int m_loop;
   unsigned int m_maxL1Id;
   unsigned int m_eventsPerEcr;

   std::chrono::time_point<std::chrono::system_clock> m_statsTime;
   bool m_connected;
   std::vector<std::string> m_otherTesters;
   std::string m_partitionName;
   IPCPartition m_ipcPartition;
   std::string m_isInfoName;
   ISInfoDictionary m_dictionary;
   bool m_syncWithIs;
   bool m_preload;
   std::vector<unsigned int> m_l1List;
};

TheTest::TheTest(int argc, char* argv[])
   :m_executable(argv[0]),
    m_generator(0),
    m_nameService(0),m_thread(0),
    m_active(false),
    m_interactive(false),
    m_startDelay(0),
    m_ebPercent(0.0),m_l2Percent(0.0),m_metPercent(0.0),m_instance(0),
    m_l1Rate(100),m_totalInstances(1),
    m_deleteGrouping(100),m_maxOutstanding(100),

    m_mbytesReceivedRunLast(0.0),m_mbytesReceivedISLast(0.0),
    m_ioService(0),
    m_generateMode(0),
    m_uncontrolled(false),m_loop(0),
    m_maxL1Id(0xffffffff),    
    m_eventsPerEcr(0),
    m_connected(false),
    m_isInfoName(""),
    m_syncWithIs(true) {

   std::string parent;
   std::string partition;
   std::string segment;
   std::string database;
   bool noSync=false;

   po::options_description desc(argv[0]);
   desc.add_options()

      ("name,n", po::value<std::string>(&m_name), "Name of this object in OKS database")

      ("interactive,i",po::bool_switch(&m_interactive), "run from command line without run controller")
      ("uncontrolled,u",po::bool_switch(&m_uncontrolled), "run without the run control ItemCtrl interface")

      ("partition,p", po::value<std::string>(&partition)->default_value(""), "Name of the partition in OKS database")
      ("segment,s", po::value<std::string>(&segment)->default_value(""), "Name of the segment in OKS database")
      ("parent,P", po::value<std::string>(&parent)->default_value(""), "Name of the parent controller object in OKS database")
      ("database,d", po::value<std::string>(&database)->default_value(""), "Name of the OKS database")

      ("address,a", po::value<std::string>(&m_address), "IP address to connect to the ReadoutApplication on")
      ("port", po::value<std::uint32_t>(&m_port)->default_value(9000), "port that the ReadoutApplication is listening on")

      ("multicastClear,c", po::value<std::string>(&m_mcAddress), "Send clears to this multicast address")

      ("ebPercent,e", po::value<float>(&m_ebPercent), "percentage of requests that apply to all channels")
      ("l2Percent,l", po::value<float>(&m_l2Percent), "percentage of requests that are for RoI only")
      ("metPercent,m", po::value<float>(&m_metPercent), "percentage of requests that are for missing Et")
      ("deleteGrouping,g",po::value<std::uint32_t>(&m_deleteGrouping),"number of l1Ids in a Clear request")

      ("startDelay",po::value<std::uint32_t>(&m_startDelay),"delay in us before generating first request")

      ("sessions,S",po::value<std::uint32_t>(&m_nSessions),"number of sessions to open")
      ("outstanding,o",po::value<std::uint32_t>(&m_maxOutstanding),"max number of outstanding requests")
//      ("firstEvent,f",po::value<std::uint32_t>(&firstEvent)->default_value(0),"L1 ID of 1st fragment to request")
      ("threads,t",po::value<std::uint32_t>(&m_nThreads)->default_value(1),"Number of ioService threads per DataRequester")
      ("rate,r",po::value<std::uint32_t>(&m_l1Rate),"Target L1 rate in kHz")

      ("instance,I",po::value<std::uint32_t>(&m_instance),"Number of this instance of ROSTester")
      ("totalInstances,T",po::value<std::uint32_t>(&m_totalInstances),"Total number of ROSTester instances in system")
      ("noSync",po::bool_switch(&noSync),"don't sync with other ROSTesters via IS")

      ("numberOfRequests,N",po::value<std::uint32_t>(&m_nRequests)->default_value(1000000),"number of requests to generate")

      ("printRequest,R",po::bool_switch(&m_printReq), "print out a summary of each request sent")
      ("printData,D",po::bool_switch(&m_printData), "print out a summary of each data packet received")
      ("initialL1",po::value<std::uint32_t>(&m_initialL1)->default_value(0),"L1Id to start on")
      ("preloaded",po::bool_switch(&m_preload), "Load list of L1 IDs from file")
      ("eventsPerEcr,E",po::value<uint32_t>(&m_eventsPerEcr), "number of events in each ECR block")
      ;
  
   try {
      po::store(po::parse_command_line(argc, argv, desc), m_config);
      po::notify(m_config);
   }
   catch (std::exception& ex) {
      std::cerr << desc << std::endl;
      exit(EXIT_FAILURE);
   }

   m_syncWithIs=!(noSync|m_preload);

   IPCCore::init(argc,argv);

   resetStatistics();
}

TheTest::~TheTest() noexcept {
   if (m_generator) {
      delete m_generator;
   }
   if (m_isInfoName!="") {
      try {
         m_dictionary.remove(m_isInfoName);
      }
      catch (daq::is::Exception& exc) {
         // Ignore error at this stage.  It may be that somebody removed the item behind our back
      }
   }
}

bool TheTest::controlled() {
   return !m_uncontrolled;
}

void TheTest::configure(const daq::rc::TransitionCmd&){
   std::cout << "configure entered\n";
   // Get config from OKS database
   Configuration confDB("");
   if(!confDB.loaded()) {
      std::cerr << m_executable << ": cannot load database <" << confDB.get_impl_spec() << ">" <<std::endl;
      exit(EXIT_FAILURE);
   }

   const daq::core::Partition* dbPartition;
   std::string any;
   if(!(dbPartition = daq::core::get_partition( confDB, any))) {
      std::cerr << m_executable << ": TDAQ_PARTITION not set or Partition object not found in the DB" <<std::endl;
      exit(EXIT_FAILURE);
   }

   const daq::df::ROSTester* dbApp= confDB.get<daq::df::ROSTester>(m_name);
   if (dbApp==0) {
      std::cerr << m_executable << ": Cannot find " << m_name << " in database" <<std::endl;
      exit(EXIT_FAILURE);
   }

   // Ask that all $variables in the DB are automatically substituted
   confDB.register_converter(new daq::core::SubstituteVariables(*dbPartition));


   m_roiDistribution=dbApp->get_ROIDistribution();
   if (m_config.count("l2Percent") ==0) {
      m_l2Percent=dbApp->get_L2RequestFraction();
   }
   if (m_config.count("ebPercent") ==0) {
      m_ebPercent=dbApp->get_EBRequestFraction();
   }
   if (m_config.count("deleteGrouping") ==0) {
      m_deleteGrouping=dbApp->get_DeleteGrouping();
   }
   if (m_config.count("sessions")==0) {
      m_nSessions=dbApp->get_Sessions();
   }
   if (m_config.count("outstanding")==0) {
      m_maxOutstanding=dbApp->get_Outstanding();
   }
   if (m_config.count("metPercent")==0) {
      m_metPercent=dbApp->get_MeTPercent();
   }
   if (m_config.count("rate")==0) {
      m_l1Rate=dbApp->get_L1Rate();
   }
   if (m_config.count("startDelay")==0) {
      m_startDelay=dbApp->get_StartDelay();
   }

   m_maxL1Id=dbApp->get_MaxL1();

   const daq::df::ROS* myROS=dbApp->get_ConnectsToROS();
   if (myROS==0) {
      std::cerr << "Failed to find ConnectsToROS relationship in database\n";
      exit(EXIT_FAILURE);
   }
   m_rosName=myROS->UID();

   std::cout << "Searching for ROSTesters that connect to " << m_rosName << std::endl;

   std::set<std::string> apptype;
   apptype.insert("ROSTester");
   auto testerApps=dbPartition->get_all_applications(&apptype);

   std::cout << "Got " << testerApps.size() << " applications from get_all_applications()\n";
   std::vector<const daq::df::ROSTester*> testers;
   for (auto app : testerApps) {
      const daq::df::ROSTester* t=
         confDB.cast<daq::df::ROSTester>(app);
      if (t!=0){
         testers.push_back(t);
      }
   }

   std::cout << "Found " << testers.size() << " ROStesters\n";
   unsigned int instance=0;
   for (unsigned int index=0;index<testers.size();index++) {
      std::cout << testers[index]->UID() ;
      if (testers[index]->get_ConnectsToROS()->UID()==m_rosName //&& !tIter->disabled(*dbPartition)
         ) {
         std::cout << "  <====";
         if (testers[index]->UID()==m_name) {
            if (m_config.count("instance")==0) {
               m_instance=instance;
            }
            if (!m_preload && m_config.count("initialL1")!=0) {
               m_instance+=m_initialL1;
            }
         }
         else {
            m_otherTesters.push_back(testers[index]->UID());
         }
         instance++;
      }
      std::cout << std::endl;
   }
   if (m_config.count("totalInstances")==0) {
      m_totalInstances=instance;
   }

   std::cout << m_name
             << ": instance " << m_instance
             << " of " << m_totalInstances << std::endl;

   // Sort out network addresses/ports
   // =================================
   if (m_config.count("address")==0) {
      m_address="127.0.0.1";
   }
   m_endPoint=boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(m_address),m_port);

   m_partitionName=dbPartition->UID();
   m_ipcPartition=IPCPartition(m_partitionName.c_str());
   m_dictionary=ISInfoDictionary(m_ipcPartition);


   m_isInfoName=dbApp->get_ISServer()+".ROSTester."+m_name;

   std::cout << "partitionName=<" << m_partitionName << ">.  m_isInfoName=<" << m_isInfoName << ">\n";

   const daq::df::DFParameters* dfPars=
      confDB.cast<daq::df::DFParameters>(dbPartition->get_DataFlowParameters());
   if (m_config.count("address")==0 || m_port==0) {
      std::vector<std::string> networks=dfPars->get_DefaultDataNetworks();
      std::cout << "Network list:\n";
      for (auto niter=networks.begin(); niter!=networks.end(); niter++) {
         std::cout << (*niter) << std::endl;
      }
      m_nameService=new daq::asyncmsg::NameService (m_ipcPartition,
                                                    networks);
   }


   // Sort out channel list
   // ======================
   std::cout << "configure getting channel list\n";
   unsigned int detectorId=myROS->get_Detector()->get_LogicalId();
   // Work out what channels are active in the ROS we're gonna talk to
   std::vector<const daq::core::Component*> disabledComponents = dbPartition->get_Disabled();

   for (std::vector<const daq::core::ResourceBase*>::const_iterator moduleIter = myROS->get_Contains().begin(); 
        moduleIter != myROS->get_Contains().end(); ++moduleIter) {
      const daq::df::ReadoutModule*  roModule=
         confDB.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*moduleIter);
      if (roModule==0) {
         std::cerr << "ReadoutApplication " << myROS->UID()
                   << " Contains relationship to something (" << (*moduleIter)->class_name() <<
            ") that is not a ReadoutModule\n";
         continue;
      }

      const daq::core::ResourceSet*  resources=
         confDB.cast<daq::core::ResourceSet, daq::core::ResourceBase> (*moduleIter);
      if (resources==0) {
         std::cerr << "ReadoutApplication " << myROS->UID()
                   << " has a ReadoutModule that is not a ResourceSet\n";
         continue;
      }
      for (std::vector<const daq::core::ResourceBase*>::const_iterator channelIter = resources->get_Contains().begin(); 
           channelIter != resources->get_Contains().end(); ++channelIter) {
         const daq::df::InputChannel* channelPtr=
            confDB.cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
         if (channelPtr==0) {
            std::cerr << "ReadoutApplication " << myROS->UID()
                      << " Contains relationship to something (" << (*channelIter)->class_name() <<
               ") that is not an InputChannel\n";
            continue;
         }
         else {
            if (!(*channelIter)->disabled(*dbPartition)) {
               unsigned int channelId=channelPtr->get_Id();
               if ((*moduleIter)->class_name() == "RobinReadoutModule") {
                  // fold in detector ID from ROS
                  channelId=(channelId & 0x0000ffff) | (detectorId << 16);
               }
               std::cout << "Pushing back rolID " << std::hex << channelId << std::dec << std::endl;
               m_channelList.push_back(channelId);
            }
         }
      }
   }


   std::cout << "configure setting up multicast address\n ";
   if (m_config.count("multicastClear")==0) {
      m_mcAddress=dfPars->get_MulticastAddress();
   }

   m_proto=m_mcAddress.substr(0,m_mcAddress.find(":"));
   if (!m_proto.empty()) {
      m_mcAddress=m_mcAddress.substr(m_mcAddress.find(":")+1);
   }
   else {
      m_proto="tcp";
   }

   m_ioService=new boost::asio::io_service [m_nSessions];

   m_clearIOService=new boost::asio::io_service;

   std::cout << "configure Setting up ClearWork\n ";
   m_clearWork.reset(new boost::asio::io_service::work(*m_clearIOService));

   std::cout << "configure Setting up ClearThread\n ";
   m_clearThread.reset(new boost::thread
                       (boost::bind(&boost::asio::io_service::run,
                                    m_clearIOService)));
   pthread_setname_np(m_clearThread->native_handle(),"ROSTester-Clear");


   m_generateMode=1;
   m_statsTime=std::chrono::system_clock::now();

   if(m_preload) {
      auto fileVec=
         confDB.cast<daq::df::DFParameters, daq::core::DataFlowParameters>(
            dbPartition->get_DataFlowParameters())->get_UsesDataFiles();
      for (auto fileIter : fileVec) {
         auto fileName=fileIter->get_FileName();
         struct stat statBuf;
         std::string splitDir=fileName+".per-rob";
         if (stat(splitDir.c_str(),&statBuf) == 0) {
            std::ostringstream fnStream;
            fnStream << splitDir << "/0x" << std::setfill('0')
                     << std::setw(8) << std::uppercase << std::hex
                     << m_channelList[0] << ".data";
            fileName=fnStream.str();
         }
         std::cout << "Loading data from " << fileName << std::endl;
         DataReader* reader=pickDataReader(fileName);
         while (reader->good()) {
            unsigned int eventSize;
            char* buffer;
            DRError errorCode = reader->getData(eventSize,&buffer);
            if (errorCode==DROK) {
               //std::cout << "read event size " << eventSize << std::endl;
               unsigned int* header=reinterpret_cast<uint32_t*>(buffer);
               if (header[0]==0xaa1234aa) {
                  const eformat::FullEventFragment<const uint32_t*> fragment(header);
                  m_l1List.push_back(fragment.lvl1_id());
               }
            }
         }
      }
      std::sort(m_l1List.begin(),m_l1List.end());
      std::cout << "Loaded " << m_l1List.size() << " events from file\n";
   }
}

void TheTest::connect(const daq::rc::TransitionCmd&){

   if (m_proto=="tcp") {
      // use TCP
      if (m_nameService) {
         m_clearSession = std::make_shared<UnicastClear>(m_deleteGrouping,
                                                         *m_clearIOService,
                                                         m_nameService);
      }
      else {
         m_clearSession = std::make_shared<UnicastClear>(m_deleteGrouping,
                                                         *m_clearIOService,
                                                         &m_endPoint);
      }
   }
   else {
      auto sPos=m_mcAddress.find("/");
      std::string network;
      if (sPos!=std::string::npos) {
         network=m_mcAddress.substr(sPos+1);
      }
      m_mcAddress=m_mcAddress.substr(0,sPos);
      std::cout << "configuring mcast address " << m_mcAddress << " on network " << network << std::endl;
      m_clearSession = std::make_shared<MulticastROSClear>
         (m_deleteGrouping, *m_clearIOService, m_mcAddress, network);
   }

   m_clearSession->connect();

   m_generator=new RequestGenerator (m_channelList,
                                     m_roiDistribution,
                                     m_l2Percent,
                                     m_ebPercent,
                                     m_metPercent,
                                     m_deleteGrouping,
                                     m_l1Rate,
                                     m_maxL1Id,
                                     m_eventsPerEcr,
                                     m_maxOutstanding,
                                     m_syncWithIs,
                                     m_preload,
                                     m_partitionName,
                                     m_otherTesters,
                                     m_name,
                                     m_clearSession);

   if (m_preload){
      m_generator->setL1List(m_l1List);
   }
   m_generator->setInstance(m_instance,m_totalInstances);

   if (m_config.count("address")==0 || m_port==0) {
      boost::asio::ip::tcp::endpoint lookup;
      try {
         lookup=m_nameService->resolve(m_rosName);
         std::cout << "Endpoint=" << lookup << std::endl;
      }
      catch (daq::asyncmsg::CannotResolve& exc) {
         ers::error(exc);
      }
      catch (daq::is::RepositoryNotFound& exc) {
         ers::error(exc);
      }

      if (m_config.count("address")==0) {
         m_endPoint=lookup;
      }
      else {
         m_endPoint=boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address::from_string(m_address),
            lookup.port());
      }
   }


   int maxOutstandingPerSession=m_maxOutstanding/m_nSessions;
   if (maxOutstandingPerSession==0){
      maxOutstandingPerSession=1;
   }
   int outstandingUnblock=maxOutstandingPerSession/2;
//   if (outstandingUnblock==0) 
   std::cout << "Creating DataRequesters with "
                << m_ebPercent << "% EB, " << m_l2Percent <<"% L2\n";
   for (uint32_t session=0; session<m_nSessions; session++) {
      std::ostringstream nameStream;
      nameStream << "DataRequester-" << session;
      m_requester.push_back(std::make_shared<DataRequester>(m_ioService[session],
                                                          m_printReq,
                                                          m_printData,
                                                          m_nThreads,
                                                          maxOutstandingPerSession,
                                                          outstandingUnblock,
                                                          m_generator));

      std::cout << "connecting to endpoint " << m_endPoint << std::endl;
      m_requester[session]->connect(m_endPoint,nameStream.str());
   }

   m_connected=true;
}

void TheTest::disconnect(const daq::rc::TransitionCmd&){
   std::cout << "Disconnecting\n";
   m_connected=false;

   for (uint32_t session=0; session<m_nSessions; session++) {
      m_requester[session]->disconnect();
   }

   delete m_generator;
}

void TheTest::unconfigure(const daq::rc::TransitionCmd&){
   std::cout << "Unconfiguring\n";
   if (m_nameService) {
      delete m_nameService;
      m_nameService=0;
   }

   m_clearWork.reset();
   if (m_clearSession) {
      m_clearSession.reset();
   }

   for (auto iter : m_requester) {
      iter.reset();
   }
   m_requester.clear();


   if (m_ioService) {
      delete [] m_ioService;
      m_ioService=0;
   }

   if (m_clearIOService) {
      delete m_clearIOService;
      m_clearIOService=0;
   }

   m_channelList.clear();
   m_otherTesters.clear();
   try {
      m_dictionary.remove(m_isInfoName);
   }
   catch (daq::is::Exception& exc) {
      // Ignore error at this stage.  It may be that somebody removed the item behind our back
   }
}

void TheTest::resetStatistics(){
   m_mbytesReceivedISLast=0.0;
   m_numGeneratedLast=0;



}

void TheTest::prepareForRun(const daq::rc::TransitionCmd&){
   resetStatistics();
   m_clearSession->prepareForRun();
   m_generator->setInstance(m_instance,m_totalInstances);
   m_generator->start();
   m_generateMode=1;
   m_loop=1;
   m_active=true;
   m_thread=new std::thread(&TheTest::run,this);
   pthread_setname_np(m_thread->native_handle(),"ROSTester-run");

}

void TheTest::user(const daq::rc::UserCmd& command) {
	if (command.commandName()=="dump") {
//      std::vector<std::string> arguments=command.commandParameters();
//      for (auto argv : arguments) {
//          if (argv=="descriptors") {
//          }
//      }

      for (unsigned int session=0; session<m_nSessions; session++) {
         m_requester[session]->debug();
      }

      m_generator->dump();
   }
}

void TheTest::publish() {

   if (!m_connected) {
      return;
   }

   ROSTesterInfo myInfo;

   myInfo.l2RequestsSent=m_generator->l2RequestsGenerated();
   myInfo.ebRequestsSent=m_generator->ebRequestsGenerated();
   uint64_t numGenerated=myInfo.l2RequestsSent+myInfo.ebRequestsSent;
   myInfo.requestsSent=numGenerated;

   myInfo.ebReRequestsSent=0;
   myInfo.requestsOutstanding=0;
   myInfo.mbytesReceived=0;
   myInfo.fragmentsWithError=0;
   for (unsigned int session=0; session<m_nSessions; session++) {
      myInfo.ebReRequestsSent+=m_requester[session]->reRequests();
      myInfo.requestsOutstanding+=m_requester[session]->outstanding();
      myInfo.mbytesReceived+=m_requester[session]->megabytesReceived();
      myInfo.fragmentsWithError+=m_requester[session]->fragmentsWithError();
   }
   double deltaMBytesReceived=myInfo.mbytesReceived-m_mbytesReceivedISLast;
   m_mbytesReceivedISLast=myInfo.mbytesReceived;
   double deltaNumGenerated=numGenerated-m_numGeneratedLast;
   m_numGeneratedLast=numGenerated;
   auto now=std::chrono::system_clock::now();
   auto elapsed=now-m_statsTime;
   m_statsTime=now;
   double sec=(double)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6;
   if (sec>0) {
      myInfo.requestRate=(deltaNumGenerated/sec)/1000.0;
      myInfo.dataRate=deltaMBytesReceived/sec;
   }

   myInfo.wrapCount=m_generator->wrapCount();
   if (m_interactive) {
      std::cout << myInfo;
   }
   else {
      try {
         m_dictionary.update(m_isInfoName,myInfo);
      }
      catch (daq::is::InfoNotFound& exc1) {
         try {
            m_dictionary.insert(m_isInfoName,myInfo);
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      catch (daq::is::Exception& exc) {
         // Issue a warning and carry on
         ers::warning(exc);
      }
   }
}

void TheTest::uncontrolledRun() {
   m_generator->start();
   m_generateMode=1;
   m_active=false;

   while (m_nRequests>0) {
      std::cout << "Enter number of requests: ";
      std::cout.flush();
      std::cin >> m_nRequests;
      if (m_nRequests>0){
         run();
      }
   }
}


void TheTest::stopHLT(const daq::rc::TransitionCmd&){
   m_active=false;
   m_generator->stop();
   std::cout << "stopHLT waiting for thread to join\n";
   m_thread->join();
   std::cout << "stopHLT deleting thread\n";
   delete m_thread;
}

void TheTest::run() {
   if (m_startDelay!=0) {
      std::this_thread::sleep_for(std::chrono::microseconds(m_startDelay));
   }

   m_statsTime=std::chrono::system_clock::now();
   do {
      auto start=std::chrono::system_clock::now();
      unsigned int nGenerated=m_generator->generate(m_nRequests,m_generateMode);
      if (nGenerated<m_nRequests && m_active) {
         std::cout << "Wrong number of events generated " << nGenerated << std::endl;
      }
      m_generateMode=0;

      unsigned int outstanding=0;
      double mbytesReceived=0.0;
      uint64_t fragmentsWithError=0;
      for (unsigned int session=0; session<m_nSessions; session++) {
         outstanding+=m_requester[session]->outstanding();
         mbytesReceived+=m_requester[session]->megabytesReceived();
         fragmentsWithError+=m_requester[session]->fragmentsWithError();
      }
      double deltaMBytesReceived=mbytesReceived-m_mbytesReceivedRunLast;
      m_mbytesReceivedRunLast=mbytesReceived;
      auto elapsed=std::chrono::system_clock::now()-start;
      double sec=(double)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6;
      double rate=(nGenerated/sec)/1000.0;
      double bandwidth=deltaMBytesReceived/sec;
      std::cout.precision(3);
      std::cout << "Loop " << m_loop++
                << " sent " << nGenerated << " requests,"
                << " elapsed time=" << sec << "sec, "
                << "(" << rate << "kHz) "
                << "(" << bandwidth << "MB/s) "
                << outstanding << " requests still outstanding\n";
      std::cout.flush();
   } while (m_active);


   unsigned int outstanding;
   unsigned int nwaits=0;
   do {
      outstanding=0;
      for (unsigned int session=0; session<m_nSessions; session++) {
         outstanding+=m_requester[session]->outstanding();
      }
      if (outstanding!=0) {
         if (nwaits++%100==0) {
            std::cout << "Waiting for " << outstanding << " outstanding requests\n";
         }
         std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
   } while (outstanding!=0);

}

int main(int argc, char* argv[]){
//    for (int i=0;i<argc; i++) {
//       std::cout << "argv[" << i << "]=" << argv[i] << std::endl;
//    }
   std::shared_ptr<TheTest> test=std::make_shared<TheTest>(argc,argv);

   if (test->controlled()) {
      daq::rc::CmdLineParser parser(argc,argv,false);
      daq::rc::ItemCtrl myItem(parser, test); 
      myItem.init();
      myItem.run();
   }
   else {
      daq::rc::TransitionCmd cmd(daq::rc::FSM_COMMAND::INIT_FSM); // Dummy run control transition command not used by any RobinNPReadoutModule methods
      test->configure(cmd);

      test->connect(cmd);

      test->uncontrolledRun();
   }
}
